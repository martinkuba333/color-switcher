import React, { Component } from "react";
import ReactDOM from "react-dom";
import {CONST} from "./constants";
import {Text} from "./styled";

export class InfoText extends Component {
	constructor() {
		super();
		this.state = {
		};
	}

	render() {
		let {colorIsChosen, text} = this.props;
		let styleColor = "black";

		if (!text) {
			text = colorIsChosen ? CONST.INFO_COLOR_CHOSEN[1] : CONST.INFO_COLOR_NOT_CHOSEN[1];
			styleColor = colorIsChosen ? CONST.INFO_COLOR_CHOSEN[0] : CONST.INFO_COLOR_NOT_CHOSEN[0];
		}

		return (
			<React.Fragment>
				<Text
					color={styleColor}
				>
					{text}
				</Text>
			</React.Fragment>
		);
	}
}
