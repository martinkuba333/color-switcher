import styled from "styled-components";

export const Board = styled.div`
	width: ${props => props.size}px;
	height: ${props => props.size}px;
	display:block;
	margin-left:auto;
	margin-right:auto;
`;

export const Panel = styled.div`
	width: ${props => props.width};
	height: ${props => props.height};
	display:block;
	margin-left:auto;
	margin-right:auto;
	display: flex;
	justify-content: space-between;
	margin-top:50px;
	position:relative;
`;

export const ShapeObject = styled.div`
	width: ${props => props.shapeWidth};
	height: ${props => props.shapeHeight};
	margin: 1px;
	display: block;
	float: left;
	pointer-events: ${props => props.clickable ? "all" : "none"}
	
	&:hover {
		cursor:pointer;
	}
`;

export const Title = styled.h3`
	text-align:center;
	text-shadow: 0 0 15px gray;
	color: ${props => props.color};
`;

export const Attempts = styled.p`
	text-align:center;
	display:block;
	font-size:14px;
	margin-top:20px;
`;

export const Text = styled.p`
	text-align:center;
	display:block;
	font-size:14px;
	margin-top:20px;
	color:${props => props.color}
`;

export const Restart = styled.button`
	&:hover {
		cursor:pointer;
	}
`;
