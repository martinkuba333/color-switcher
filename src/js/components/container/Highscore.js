import React, { Component } from "react";
import ReactDOM from "react-dom";
import {CONST} from "./constants";
import {Text} from "./styled";
import {InfoText} from "./InfoText";

export class Highscore extends Component {
	constructor() {
		super();
		this.state = {
		};
	}

	createScore = () => {
		const {winPoints} = this.props;
		let winLines = [];

		for (let i = 0; i < winPoints.length; i++) {
			winLines.push(
				<InfoText
					text={winPoints[i]}
					key={i}
				/>
			)
		}

		return winLines;
	};

	render() {
		return (
			<React.Fragment>
				<InfoText
					text={CONST.HIGH_SCORE_TEXT}
				/>
				{this.createScore()}
			</React.Fragment>
		);
	}
}
