export const CONST = {
	GAME_NAME: "COLOR SWITCHER",
	TITLE_STARTING_COLOR: "gold",
	INFO_COLOR_NOT_CHOSEN: ["#b10606", "Vyberte kocku zhora, tam začnete"],
	INFO_COLOR_CHOSEN: ["green", "Kliknite na kocku dolu a zmeníte vašu kocku a všetky susedné kocky rovnakej farby"],
	
	SHAPE_NUM: 100,
	SHAPES_IN_LINE: 10,
	SHAPE_COLORS: ["red", "green", "blue", "yellow", "black", "white", "purple"],
	BOARD_SHAPE_SIZE: "9%",
	PANEL_SHAPE_HEIGHT: "20px",
	
	BOARD_SIZE:280,
	BOARD_ID: "gameBoard",
	
	WIN_TIMEOUT_WAIT: 50,
	
	HIGH_SCORE_TEXT: "HIGH SCORE",
	HIGH_SCORE_LINES: 5,
};

function findStyle(id, wantedStyle) {
  const ele = document.getElementById(id);
  let result = window.getComputedStyle(ele).getPropertyValue(wantedStyle);
  return parseInt(result);
}
