import React, { Component } from "react";
import ReactDOM from "react-dom";
import {CONST} from "./constants";
import {ShapeObject} from "./styled";

export class Shape extends Component {
	constructor() {
		super();
		this.state = {
		};
	}

	handleClick = () => {
		if (this.props.currentColor === this.props.bgColor) {
			return;
		}
		this.props.onClick(this.props.bgColor, this.props.dataId);
	};

	render() {
		const {bgColor, dataId, clickable, panelShape} = this.props;
		const style = {background: bgColor};
		const width = CONST.BOARD_SHAPE_SIZE;
		const height = panelShape ? CONST.PANEL_SHAPE_HEIGHT : CONST.BOARD_SHAPE_SIZE;

		return (
			<React.Fragment>
				<ShapeObject
					style={style}
					data-id={dataId}
					clickable={clickable}
					onClick={this.handleClick}
					shapeWidth={width}
					shapeHeight={height}
				/>
			</React.Fragment>
		);
	}
}

