import React, { Component } from "react";
import ReactDOM from "react-dom";
import {CONST} from "./constants";
import {Title} from "./styled";

export class MainTitle extends Component {
	constructor() {
		super();
		this.state = {
		};
	}

	render() {
		const {bgColor, dataId, clickable, color} = this.props;

		return (
			<React.Fragment>
				<Title
					color={color !== null ? color : CONST.TITLE_STARTING_COLOR}
				>
					{CONST.GAME_NAME}
				</Title>
			</React.Fragment>
		);
	}
}

