import React, { Component } from "react";
import ReactDOM from "react-dom";
import {CONST} from "./constants";
import {Shape} from "./Shape";
import {MainTitle} from "./MainTitle";
import {Board, Panel, Attempts, Restart} from "./styled";
import {InfoText} from "./InfoText";
import {Highscore} from "./Highscore";

class Main extends Component {
  constructor() {
    super();
    this.state = {
    	shapesColors: [],
    	colorIsChosen: false,
			startingColor: null,
			currentColor: null,
			startingShapeId: null,
			clickAttempts: 0,
			convertedShapes: null,
			winPoints: [],
    };
  }

  drawContent = () => {
  	const {shapesColors, colorIsChosen} = this.state;
  	let shape = [];

  	for (let i = 0; i < CONST.SHAPE_NUM; i++) {
  		if (shapesColors.length !== CONST.SHAPE_NUM) {
  			shapesColors.push(
  				CONST.SHAPE_COLORS[Math.floor(Math.random() * CONST.SHAPE_COLORS.length)]
				)
			}

  		shape.push(
  			<Shape
					key={i}
					dataId={i}
					bgColor={shapesColors[i]}
					clickable={!colorIsChosen}
					onClick={this.chooseStartingShape}
					panelShape={false}
				/>
			);
		}

  	return shape;
	};

  chooseStartingShape = (color, shapeId) => {
		this.setState({
			colorIsChosen: true,
			startingColor: color,
			currentColor: color,
			startingShapeId: shapeId,
			convertedShapes: [shapeId],
		})
	};

  createBottomButtons = () => {
  	let buttons = [];

  	for (let i = 0; i < CONST.SHAPE_COLORS.length; i++) {
  		buttons.push(
				<Shape
					key={i}
					dataId={i}
					bgColor={CONST.SHAPE_COLORS[i]}
					clickable={this.state.colorIsChosen}
					onClick={this.changeColors}
					panelShape={true}
					currentColor={this.state.currentColor}
				/>
			)
		}

  	return buttons;
	};

	changeColors = (color) => {
		let {clickAttempts} = this.state;
		let convertedShapes = [];

		for (let i = 0; i < CONST.SHAPE_NUM; i++) {
			convertedShapes = this.processChange(i, color);
		}

		for (let i = CONST.SHAPE_NUM - 1; i >= 0; i--) {
			convertedShapes = this.processChange(i, color);
		}
		
		clickAttempts++;
		
		this.setState({
			currentColor: color,
			clickAttempts: clickAttempts,
			convertedShapes: convertedShapes,
		}, () => {
			this.checkWin(color, clickAttempts);
		})
	};

	processChange = (i, color) => {
		const {currentColor} = this.state;
		let {shapesColors, convertedShapes} = this.state;
		const allShapes = document.querySelectorAll("[data-id]");

		if (allShapes[i].style.background === currentColor) {
			for (let j = 0; j < convertedShapes.length; j++) {
				if ((convertedShapes[j] === i + 1 && i % CONST.SHAPES_IN_LINE !== CONST.SHAPES_IN_LINE - 1) ||
						(convertedShapes[j] === i - 1 && i % CONST.SHAPES_IN_LINE !== 0) ||
						(convertedShapes[j] === i + CONST.SHAPES_IN_LINE) ||
						(convertedShapes[j] === i - CONST.SHAPES_IN_LINE) ||
						(convertedShapes.length === 1 && convertedShapes[j] === i) //if only one
				) {
					shapesColors[i] = color;

					if (convertedShapes.indexOf(i) === -1) {
						convertedShapes.push(i);
					}
				}
			}
		}

		return convertedShapes;
	};

	checkWin = (color, clickAttempts) => {
		const count = document.querySelectorAll("#" + CONST.BOARD_ID + " [style*='" + color + "']").length;

		if (count >= CONST.SHAPE_NUM) {
			const self = this;
			setTimeout(function() {
				alert("WIN!! You make it in " + clickAttempts + " turns");
				self.restart(clickAttempts);
			}, CONST.WIN_TIMEOUT_WAIT);
		}
	};

	restartButton = () => {
		return (
			<React.Fragment>
				<Restart
					onClick={() => this.restart(Number.POSITIVE_INFINITY)}
				>
					Restart
				</Restart>
			</React.Fragment>
		)
	};

	restart = (clickAttempts) => {
		let winPoints = this.state.winPoints;
		winPoints.push(clickAttempts);

		winPoints = this.keepOnlyBestScore(winPoints);

		this.setState({
			shapesColors: [],
			colorIsChosen: false,
			startingColor: null,
			currentColor: null,
			startingShapeId: null,
			clickAttempts: 0,
			convertedShapes: null,
			winPoints: winPoints,
		})
	};

	keepOnlyBestScore = (winPoints) => {
		let result = [];

		for (let i = 0; i < CONST.HIGH_SCORE_LINES; i++) {
			let index = winPoints.indexOf(Math.min(...winPoints));

			if (index !== -1) {
				if (winPoints[index] !== Infinity) {
					result.push(winPoints[index]);
				}

				winPoints.splice(index, 1);
			}
		}

		return result;
	};

  render() {
  	const {currentColor, colorIsChosen, clickAttempts, winPoints} = this.state;
  	
    return (
      <React.Fragment>
				<MainTitle
					color={currentColor}
				/>
				<Board
					id={CONST.BOARD_ID}
					size={CONST.BOARD_SIZE}
				>
					{this.drawContent()}
				</Board>
				<InfoText
					colorIsChosen={colorIsChosen}
				/>
				<Panel>
					{this.createBottomButtons()}
					{this.restartButton()}
				</Panel>
				<Attempts>
					{clickAttempts}
				</Attempts>
				<Highscore
					winPoints={winPoints}
				/>
      </React.Fragment>
    );
  }
}
export default Main;

board ? ReactDOM.render(<Main />, board) : false;
